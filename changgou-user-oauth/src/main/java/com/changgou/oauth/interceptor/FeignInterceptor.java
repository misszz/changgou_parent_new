package com.changgou.oauth.interceptor;

import com.changgou.oauth.util.AdminToken;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhongxuwang
 * @since 2021-04-15 10:34 上午
 */

@Configuration
public class FeignInterceptor implements RequestInterceptor {
    //令牌头名字
    private static final String AUTHORIZE_TOKEN = "Authorization";
    /**
     * 在feign调用其他微服务之前进行拦截
     *
     * 在认证微服务中请求查询用户信息（用户微服务中）
     * 1.没有令牌，在feign调用之前，生成令牌
     * 2.将令牌携带过去
     * 3.将令牌存放到header中，传递给下一个微服务
     * 在feign调用之前，创建令牌,并将令牌封装到请求头中，传递给下一个微服务
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        //生成admin令牌
        String token = AdminToken.adminToken();
        requestTemplate.header(AUTHORIZE_TOKEN,"bearer "+token);
//        try {
//            //使用RequestContextHolder工具获取到request的相关对象
//            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//            if (requestAttributes != null){
//                HttpServletRequest request = requestAttributes.getRequest();
//                //获取所有请求的key
//                Enumeration<String> headers = request.getHeaderNames();
//                if (headers != null){
//                    while (headers.hasMoreElements()){
//                        //头文件中的key
//                        String key = headers.nextElement();
//                        //根据key获取value
//                        String value = request.getHeader(key);
//                        //将令牌的数据放入到请求头中,feign调用其他微服务的时候携带过去
//                        requestTemplate.header(key,value);
//                    }
//                }
//            }
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
    }
}
