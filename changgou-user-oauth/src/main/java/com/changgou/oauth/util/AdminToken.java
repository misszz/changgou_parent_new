package com.changgou.oauth.util;

import com.alibaba.fastjson.JSON;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 2021-04-15 3:27 下午
 */
public class AdminToken {

    public static String adminToken(){
        //证书文件路径
        String key_location = "changgou.jks";
        //密钥库密码
        String key_password = "changgou";
        //密钥密码
        String keypwd = "changgou";
        //密钥别名
        String alias = "changgou";

        //访问证书路径
        ClassPathResource resource = new ClassPathResource("changgou.jks");
        //创建秘钥工厂
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, key_password.toCharArray());

        //读取密钥对（公钥和私钥）
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias, keypwd.toCharArray());
        //获取私钥
        PrivateKey rsaPrivate = keyPair.getPrivate();

        //定义payload
        Map<String, Object> tokenMap = new HashMap<>();
//        tokenMap.put("id", "1");
        tokenMap.put("name", "itheima");
        tokenMap.put("authorities", new String[]{"admin","user"});

        //生成jwt令牌
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(tokenMap), new RsaSigner((RSAPrivateKey) rsaPrivate));

        //取出令牌
        String encoded = jwt.getEncoded();
        return encoded;
    }
}
