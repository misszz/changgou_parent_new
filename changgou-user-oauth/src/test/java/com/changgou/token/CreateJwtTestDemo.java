package com.changgou.token;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 7:24 下午
 */
public class CreateJwtTestDemo {

    /**
     * 创建令牌
     */
    @Test
    public void testCreateToken(){
        //证书文件路径
        String key_location = "changgou.jks";
        //密钥库密码
        String key_password = "changgou";
        //密钥密码
        String keypwd = "changgou";
        //密钥别名
        String alias = "changgou";

        //访问证书路径
        ClassPathResource resource = new ClassPathResource("changgou.jks");
        //创建秘钥工厂
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, key_password.toCharArray());

        //读取密钥对（公钥和私钥）
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias, keypwd.toCharArray());
        //获取私钥
        PrivateKey rsaPrivate = keyPair.getPrivate();

        //定义payload
        Map<String, Object> tokenMap = new HashMap<>();
        tokenMap.put("id", "1");
        tokenMap.put("name", "itheima");
        tokenMap.put("roles", "ROLE_VIP,ROLE_USER");

        //生成jwt令牌
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(tokenMap), new RsaSigner((RSAPrivateKey) rsaPrivate));

        //取出令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);


    }


    /**
     * 校验令牌
     */
    @Test
    public void checkToken(){


        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6IlJPTEVfVklQLFJPTEVfVVNFUiIsIm5hbWUiOiJpdGhlaW1hIiwiaWQiOiIxIn0.SttapbzKXqj2zuKFkxgqUZM_DQu5E6K3lmjAwJPuUuG_hCgiRS66rR5PVwh4gBPXiI5Xf-HYYAdyK5rURXa7QWJg6nr398Xz9iCGbo6jBc3z5HPk5DchNP3kp8vMObeFpPWqSbdYMF9bECejweclrX3dN7OOKBiWchbC1GRKUBQsZnhD-1TUbzurNLOKJyWx9DFKlxRED3TsdIvcCieD4dt0zvU4yz2SuWEzSMUmA83P103-dK7OZ8LlrC8is2NsWo9B4BBI4pKI4JO3Q_YC6UiUr8MA2iMoJ26MNOgPzN797Ic5_so6bYiHnF7yLCGwLBuk7weJERObpRdYWRPONQ";
        //公钥
        String public_key = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiprQo14xZx3b8wVAw+eJi7U7ZrZtXEQc8pGeu/yDjBIEs0ifZUR/UcnQeRIWFJ3taHq1S1LN1qYw7liaoL9tr3t69Wr7ap1enhmfykFdyi+40p3yZLxJmutEFcK5zYXNHJyef4aYFYlcKaTPYz0vhaVazJSWDXselYkYaGMRPCFXEwWB9YFUdbeXwWBUE85WVjYKG+3xkGIw8SJbpGL2+1jILgHs5GOiNTxPCpNECeovPYr7Pi2BfXRs0IMCLF69IIEcnvyEyRlSZLuZJtPq6KmunBXWqQm+l6a4cPfQ4po3vWULlD8KfEKt/owTsRzzfssUkveDdB8gONjC7LEW7wIDAQAB-----END PUBLIC KEY-----";

        //校验jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(public_key));
        //获取jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //获取签名
//        String encoded = jwt.getEncoded();
//        System.out.println(encoded);
    }
}
