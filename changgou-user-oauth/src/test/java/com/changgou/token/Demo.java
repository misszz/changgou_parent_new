package com.changgou.token;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author zhongxuwang
 * @since 2021-04-11 2:44 下午
 */
public class Demo {

    @Test
    public void createEnocde(){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        String encode = encoder.encode("1234");
        System.out.println("encode = " + encode);

        System.out.println(encoder.matches("1234", encode));
    }


    @Test
    public void  tett(){
        String[] str = {"as", "fda", "fda"};
        System.out.println(Arrays.toString(str));

        ArrayList<String> list = new ArrayList<>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("dd");
        System.out.println(list);
    }
}
