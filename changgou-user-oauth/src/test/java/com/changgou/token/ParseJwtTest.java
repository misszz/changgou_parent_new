package com.changgou.token;

import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

/*****
 * @Author: www.itheima
 * @Date: 2019/7/7 13:48
 * @Description: com.changgou.token
 *  使用公钥解密令牌数据
 ****/
public class ParseJwtTest {

    /***
     * 校验令牌
     */
    @Test
    public void testParseToken(){
//        "authorities":["vip","user","admint"]
        //令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MjA1MDY0ODAwOCwiYXV0aG9yaXRpZXMiOlsidmlwIiwiYWRtaW4iLCJ1c2VyIl0sImp0aSI6ImNmNTRkMmNkLWI0MGQtNGRkOC1iNjIwLWNlOWJkNDVkYmU4MCIsImNsaWVudF9pZCI6ImNoYW5nZ291IiwidXNlcm5hbWUiOiJzeml0aGVpbWEifQ.SoIxdxLDJdjxj8t1mfwcVqD81uo5L78w81R3ny5_bn013LZ0R5IJ0tksfUdzC21cUBHu_grEvhT9_bQ_LDMSd0O_sDZbRMxw-z9GOwNkfexzB5StXwzeh-StN-2199hn6M7Pub7fB8_3zdSb7ufniMjVvigl7b6UguNdn5GArWE45KnIdM0b-vgbkXH9suDSW3g4ucmh8Aoe7T2jBklwnOLf4uf7f2t-d0CeEnul_q-XTEzJfULLuROutH2z6Xct5Yhr3SNuPUQ0UGUPLfcwdFHVxQBZf7YUwLy9f-1UZ6W_0NkCcau9BESuK4LT46kOClLjWpTvR-bdU-ofIX8mIA";

        //公钥
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiprQo14xZx3b8wVAw+eJi7U7ZrZtXEQc8pGeu/yDjBIEs0ifZUR/UcnQeRIWFJ3taHq1S1LN1qYw7liaoL9tr3t69Wr7ap1enhmfykFdyi+40p3yZLxJmutEFcK5zYXNHJyef4aYFYlcKaTPYz0vhaVazJSWDXselYkYaGMRPCFXEwWB9YFUdbeXwWBUE85WVjYKG+3xkGIw8SJbpGL2+1jILgHs5GOiNTxPCpNECeovPYr7Pi2BfXRs0IMCLF69IIEcnvyEyRlSZLuZJtPq6KmunBXWqQm+l6a4cPfQ4po3vWULlD8KfEKt/owTsRzzfssUkveDdB8gONjC7LEW7wIDAQAB-----END PUBLIC KEY-----";

        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));

        //获取Jwt原始内容 载荷
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }
}
