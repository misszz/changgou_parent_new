package com.changgou.common.test;

import com.alibaba.fastjson.JSON;
import com.changgou.common.pojo.JwtUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 2:27 下午
 */
public class JWTTest {


    public static void main(String[] args) {
//        String token = testCreateToken();
//        testParseToken(token);
        String token = testJwtuint();
        testJwtparse(token);
    }

    public static String testCreateToken(){
        JwtBuilder builder = Jwts.builder()
                .setId("111")
                .setAudience("wangzhx")
                .setSubject("内容主体")
                .setIssuedAt(new Date())
//                .setExpiration(new Date())
                .signWith(SignatureAlgorithm.HS256,"itcase");   //设置签名 使用HS256算法，并设置secretkey字符串

        //自定义claims
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("name","wangzhx");
        map.put("age", 23);
        builder.addClaims(map);
        //构建    并返回一个字符串
        System.out.println(builder.compact());
        return builder.compact();
    }

    public static void testParseToken(String token){
        Claims claims = Jwts.parser()
                .setSigningKey("itcase")
                .parseClaimsJws(token)
                .getBody();

        System.out.println(claims);
    }


    public static String testJwtuint(){
        String jwt = JwtUtil.createJWT("22111", JSON.toJSONString("{\"name\":\"wangzhx\",\"age\":10}"), null);
        System.out.println("jwt = " + jwt);
        return jwt;
    }

    public static void testJwtparse(String jwt){
        try {
            Claims claims = JwtUtil.parseJWT(jwt);
            System.out.println(claims);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
