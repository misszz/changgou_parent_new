package com.changgou.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author zhongxuwang
 * @since 3:06 下午
 * 请求拦截器
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {

    //令牌头名字
    private static final String AUTHORIZE_TOKEN = "Authorization";

    /**
     * 首先对请求路径进行判断  部分请求路径放行（例如用户登录）
     * 获取请求头中信息 如果为空，则输出错误代码响应405 没有认证
     * 解析token，解析错误返回401
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        String path = request.getURI().getPath();

        if (path.startsWith("/api/user/login") || path.startsWith("/api/brand/search/")){
            //放行
            Mono<Void> filter = chain.filter(exchange);
            return filter;
        }

        //如果请求头中没有，则从请求参数中获取
        String token = request.getHeaders().getFirst(AUTHORIZE_TOKEN);

        //hasToken 为true，令牌在头文件中    为false，令牌不在头文件中，将令牌封装到头文件中-》并封装给下一个微服务
        boolean hasToken = true;
        if (StringUtils.isEmpty(token)){
            hasToken = false;
            //从cookie中获取
            HttpCookie cookie = request.getCookies().getFirst(AUTHORIZE_TOKEN);
            if (cookie != null){
                token = cookie.getValue();
            }else {
                //从请求参数中获取
                token = request.getQueryParams().getFirst(AUTHORIZE_TOKEN);
            }
        }

        //如果为空，则输出错误代码
        if (StringUtils.isEmpty(token)){
            response.setStatusCode(HttpStatus.METHOD_NOT_ALLOWED);
            return response.setComplete();
        }else {
            if (!hasToken){
                if (!token.startsWith("bearer ") && !token.startsWith("Bearer ")){
                    token = "bearer " + token;
                }
                request.mutate().header(AUTHORIZE_TOKEN, token);
            }
        }

//        //解析令牌
//        try {
//            Claims claims = JwtUtil.parseJWT(token);
//            System.out.println();
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            //解析失败，响应401错误
//            response.setStatusCode(HttpStatus.UNAUTHORIZED);    //weirenz
//            return response.setComplete();
//        }
//
//        //将令牌封装到请求头中
//        request.mutate().header(AUTHORIZE_TOKEN, token);
        return chain.filter(exchange);  //放行
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
