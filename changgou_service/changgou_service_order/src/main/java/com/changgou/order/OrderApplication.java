package com.changgou.order;

import com.changgou.common.pojo.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author zhongxuwang
 * @since 2021-04-12 10:36 上午
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.changgou.order.dao"})
@EnableFeignClients(basePackages = {"com.changgou.goods.feign","com.changgou.user.feign"})
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

//    //注入feign调用之前拦截器
//    @Bean
//    public FeignInterceptor feignInterceptor(){
//        return new FeignInterceptor();
//    }

    //注入idwork主键生成
    @Bean(name = "idWorker")
    public IdWorker idWorker(){
        return new IdWorker(1,1);
    }
}
