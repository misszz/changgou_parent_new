package com.changgou.order.controller;

import com.changgou.common.pojo.Result;
import com.changgou.common.pojo.StatusCode;
import com.changgou.order.config.TokenDecode;
import com.changgou.order.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 2021-04-12 11:14 上午
 */
@RestController
@RequestMapping(value = "/cart")
@CrossOrigin
public class CardController {
    @Autowired
    private CardService cardService;
    /**
     * 加入购物车
     * @param id 商品skuid
     * @param num 商品数量
     */
    @GetMapping("/add")
    public Result add(Long id, int num){

        //获取用户登录的信息
        Map<String, String> userInfo = TokenDecode.getUserInfo();

        //解析令牌，获取用户信息
        cardService.add(id, num, userInfo.get("username"));
        return new Result(true, StatusCode.OK,"加入购物车成功");
    }



    /**
     * 查询当前登录用户的购物车列表
     */
    @GetMapping("/list")
    public Result list(){
        //获取用户登录的信息
        Map<String, String> userInfo = TokenDecode.getUserInfo();
        List list = cardService.list(userInfo.get("username"));
        return new Result(true,StatusCode.OK, "购物车列表查询成功", list);
    }
}
