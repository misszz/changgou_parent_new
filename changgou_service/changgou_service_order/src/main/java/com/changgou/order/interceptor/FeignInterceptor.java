package com.changgou.order.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author zhongxuwang
 * @since 2021-04-15 10:34 上午
 */

@Configuration
public class FeignInterceptor implements RequestInterceptor {
    //令牌头名字
    private static final String AUTHORIZE_TOKEN = "Authorization";
    /**
     * 获取用户的令牌，并在feign调用其他微服务之前，将令牌放在请求头中，传递给下一个微服务
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
            //使用RequestContextHolder工具获取到request的相关对象
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null){
                HttpServletRequest request = requestAttributes.getRequest();
                //获取所有请求的key
                Enumeration<String> headers = request.getHeaderNames();
                if (headers != null){
                    while (headers.hasMoreElements()){
                        //头文件中的key
                        String key = headers.nextElement();
                        //根据key获取value
                        String value = request.getHeader(key);
                        //将令牌的数据放入到请求头中,feign调用其他微服务的时候携带过去
                        requestTemplate.header(key,value);
                    }
                }
            }
        } catch (Exception exception) {
//            exception.printStackTrace();
        }
    }
}
