package com.changgou.order.service;

import java.util.List;

/**
 * @author zhongxuwang
 * @since 2021-04-12 11:16 上午
 */
public interface CardService {

    /**
     * 加入购物车
     */
    void add(Long id,int num,String username);

    /**
     * 购物车列表
     */
    List list(String username);
}
