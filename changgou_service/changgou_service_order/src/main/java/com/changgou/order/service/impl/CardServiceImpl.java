package com.changgou.order.service.impl;

import com.changgou.common.pojo.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.feign.SpuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.goods.pojo.Spu;
import com.changgou.order.pojo.OrderItem;
import com.changgou.order.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhongxuwang
 * @since 2021-04-12 11:17 上午
 */
@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private SkuFeign skuFeign;
    @Autowired
    private SpuFeign spuFeign;
    @Autowired
    private RedisTemplate redisTemplate;

    private static final String CART = "Cart_";  //用户购物车前缀
    /**
     * 加入购物车
     * @param id
     * @param num
     * @param username
     */
    @Override
    public void add(Long id,int num,String username) {
        if (id == null){
            throw new RuntimeException("商品不存在");
        }
        //获取商品sku
        Result<Sku> skuResult = skuFeign.findById(id);
        if (skuResult.getData() == null || skuResult.getData().getSpuId() == null){
            throw new RuntimeException("商品不存在");
        }
        if (num <= 0){
            //移出购物车中该商品
            redisTemplate.boundHashOps(CART + username).delete(id);
            //如果当前用户此时购物车为空，移出购物车
            if (redisTemplate.boundHashOps(CART + username).size() <= 0){
                //删除当前用户的key
                redisTemplate.delete(CART + username);
            }
            return;
        }
        //获取商品spu
        Result<Spu> spu = spuFeign.findById(skuResult.getData().getSpuId());
        //将sku封装成OrderItem
        OrderItem orderItem = sku2ToOrderItem(num, skuResult.getData(), spu.getData());
        //将购物车存入到redis中
        redisTemplate.boundHashOps(CART + username).put(skuResult.getData().getId(),orderItem);
    }

    private OrderItem sku2ToOrderItem(int num, Sku sku, Spu spu) {
        //封装到orderItem对象中
        OrderItem orderItem = new OrderItem();
        orderItem.setSpuId(sku.getSpuId());
        orderItem.setSkuId(sku.getId());
        orderItem.setName(sku.getName());
        orderItem.setPrice(sku.getPrice());
        orderItem.setNum(num);
        orderItem.setMoney(num*orderItem.getPrice());       //单价*数量
        orderItem.setPayMoney(num*orderItem.getPrice());    //实付金额
        orderItem.setImage(sku.getImage());
        orderItem.setWeight(sku.getWeight()*num);           //重量=单个重量*数量

        //分类ID设置
        orderItem.setCategoryId1(spu.getCategory1Id());
        orderItem.setCategoryId2(spu.getCategory2Id());
        orderItem.setCategoryId3(spu.getCategory3Id());
        return orderItem;
    }

    /**
     * 用户购物车列表
     * @param username
     */
    @Override
    public List<OrderItem> list(String username) {
        List<OrderItem> cartList = redisTemplate.boundHashOps(CART + username).values();
        return cartList;
    }
}
