package com.changgou.dao;

import com.changgou.search.pojo.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author zhongxuwang
 * @since 4:01 下午
 */
public interface SkuEsMapper extends ElasticsearchRepository<SkuInfo,Long> {

}
