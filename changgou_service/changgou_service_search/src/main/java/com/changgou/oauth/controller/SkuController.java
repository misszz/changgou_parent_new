package com.changgou.oauth.controller;

import com.changgou.common.pojo.Result;
import com.changgou.common.pojo.StatusCode;
import com.changgou.service.SkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author zhongxuwang
 * @since 1:33 下午
 */
@RestController
@RequestMapping(value = "/search")
public class SkuController {

    @Autowired
    private SkuService skuService;


    @GetMapping("/import")
    public Result importData(){
        skuService.importData();
        return new Result(true, StatusCode.OK, "导入ES成功");
    }

    @GetMapping
    public Map search(@RequestParam(required = false) Map searchMap){
        Map map = skuService.search(searchMap);
        return map;
//        return new Result(true, StatusCode.OK, "查询成功!!!",map);
    }
}
