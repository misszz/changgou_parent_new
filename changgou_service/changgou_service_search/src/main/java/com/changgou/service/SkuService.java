package com.changgou.service;

import java.util.Map;

/**
 * @author zhongxuwang
 * @since 11:56 下午
 */
public interface SkuService {
    /**
     * 导入SKU到ES中
     */
    void importData();

    /**
     * 查询
     */
    Map search(Map<String,String> searchMap);
}
