package com.changgou.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.search.pojo.SkuInfo;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 10:59 下午
 */
public class SearchResultMapperImpl implements SearchResultMapper {
    @Override
    public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> aClass, Pageable pageable) {
        List<T> list = new ArrayList<>();

        SearchHits hits = searchResponse.getHits();
        if (null != hits) {
            for (SearchHit hit : hits) {
                SkuInfo skuInfo = JSON.parseObject(hit.getSourceAsString(), SkuInfo.class);

                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                if (null != highlightFields && highlightFields.size() > 0) {
                    skuInfo.setName(highlightFields.get("name").getFragments()[0].toString());
                }
                list.add((T) skuInfo);
            }
        }
        return new AggregatedPageImpl<T>(list, pageable, hits.getTotalHits(), searchResponse.getAggregations());
    }
}
