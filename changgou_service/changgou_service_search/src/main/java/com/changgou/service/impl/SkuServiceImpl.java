package com.changgou.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.common.pojo.Result;
import com.changgou.dao.SkuEsMapper;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.search.pojo.SkuInfo;
import com.changgou.service.SkuService;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author zhongxuwang
 * @since 1:27 下午
 */
@Service
public class SkuServiceImpl implements SkuService {
    @Autowired
    private SkuFeign skuFeign;
    @Autowired
    private SkuEsMapper skuEsMapper;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 导入SKU数据
     *
     * 步骤；查询所有的SKU，将List<sku> --> 转换成 List<skuinfo>
     * @return
     */
    @Override
    public void importData() {
        //查询所有的sku
        Result<List<Sku>> result = skuFeign.findAll();
        //将List<Sku> -> 转换成 List<Skuinfo>
        List<SkuInfo> skuInfos =(List<SkuInfo>) JSON.parseArray(JSON.toJSONString(result.getData()), SkuInfo.class);

//        skuInfos.forEach(skuInfo -> skuInfo.setSpecMap(JSON.parseObject(skuInfo.getSpec())));
        for (SkuInfo skuInfo : skuInfos) {
            skuInfo.setSpecMap(JSON.parseObject(skuInfo.getSpec()));
        }
        skuEsMapper.saveAll(skuInfos);
    }

    /**
     * 查询
     * @param searchMap
     * @return
     */
    @Override
    public Map search(Map<String, String> searchMap) {


        //构造查询对象
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();

        //构造查询条件
        buildSearchCondition(queryBuilder,searchMap);


        //根据商品分类进行分组
        queryBuilder.addAggregation(AggregationBuilders.terms("skuCategorygroup").field("categoryName").size(10000));
        //根据商品品牌分组
        queryBuilder.addAggregation(AggregationBuilders.terms("skuBrandgroup").field("brandName").size(10000));
        /*
        * 根据商品规格进行分组    [{"电视音响效果":"小影院","电视屏幕尺寸":"20英寸","尺码":"165"},
        *                       {"电视音响效果":"立体声","电视屏幕尺寸":"20英寸","尺码":"170"},
        *                       {"电视音响效果":"立体声","电视屏幕尺寸":"60英寸","尺码":"170"}]
        *
        * 主要是做去重操作，防止脏数据
        * */
        queryBuilder.addAggregation(AggregationBuilders.terms("skuSpecgroup").field("spec.keyword"));

        //分页搜索
        searchPage(searchMap, queryBuilder);

        //构建排序查询
        String sortRule = searchMap.get("sortRule");
        String sortField = searchMap.get("sortField");
        if (!StringUtils.isEmpty(sortRule) && !StringUtils.isEmpty(sortField)){
            queryBuilder.withSort(SortBuilders.fieldSort(sortField).order(sortRule.equals("DESC")? SortOrder.DESC:SortOrder.ASC));
        }

        //创建查询对象
//        NativeSearchQuery query = queryBuilder.build();

        //高亮查询
        //执行查询
        AggregatedPage<SkuInfo> skuPage = elasticsearchTemplate.queryForPage(queryBuilder.build(), SkuInfo.class);

        //指定高亮域，也就是设置哪个域需要高亮显示
        //设置高亮条件
//        queryBuilder.withHighlightFields(new HighlightBuilder.Field("name"));
//        queryBuilder.withHighlightBuilder(new HighlightBuilder().preTags("<em style=\"color:red\">").postTags("</em>"));
//        HighlightBuilder.Field field = new HighlightBuilder
//                .Field("name")
//                .preTags("<span style='color:red'>")
//                .postTags("</span>");
//        queryBuilder.withHighlightFields(field);
//
//
//        AggregatedPage<SkuInfo> skuPage = elasticsearchTemplate.queryForPage(queryBuilder.build(),
//                SkuInfo.class,
//                new SearchResultMapperImpl());

        //返回结果
        HashMap<String, Object> resultMap = new HashMap<>();

        //获取商品分类分组结果
        if (searchMap == null || searchMap.get("category") == null){
            StringTerms skuCategorygroupTerms =(StringTerms) skuPage.getAggregation("skuCategorygroup");
            List<String> categoryList = getStringCategoryList(skuCategorygroupTerms);
            resultMap.put("categoryList",categoryList);
        }


        if (searchMap == null || searchMap.get("brand") == null) {
            //获取商品品牌分组结果
            StringTerms skuBrandgroupTerms = (StringTerms) skuPage.getAggregation("skuBrandgroup");
            List<String> brandList = getStringBrandList(skuBrandgroupTerms);
            resultMap.put("brandList",brandList);
        }

        //获取商品规格分组结果
        StringTerms skuSpecgroupTerms =(StringTerms) skuPage.getAggregation("skuSpecgroup");
        Map specMap = getStringSpecMap(skuSpecgroupTerms);

        resultMap.put("specMap",specMap);
        resultMap.put("rows", skuPage.getContent());
        resultMap.put("total", skuPage.getTotalElements());
        resultMap.put("totalPages", skuPage.getTotalPages());
        return resultMap;
    }

    private void searchPage(Map<String, String> searchMap, NativeSearchQueryBuilder queryBuilder) {
        //构造分页对象
        int size = 2;   //后台定义每页展示的数据
        if (!StringUtils.isEmpty(searchMap.get("pageNum"))){
           //默认从第一页开始查询
            int pageNum = 1;
            try {
                pageNum = Integer.parseInt(searchMap.get("pageNum"));
            } catch (NumberFormatException e) {
                pageNum = 1;
            }
            queryBuilder.withPageable(PageRequest.of(pageNum - 1,size));
        }
    }


    /**
     * 构造组合查询条件
     * @param queryBuilder
     * @param searchMap 搜索查询条件
     */
    private void buildSearchCondition(NativeSearchQueryBuilder queryBuilder,Map<String, String> searchMap) {
        if (searchMap != null){
            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            //获取关键字
            String keywords = searchMap.get("keywords");
            //根据关键字查询
            if (!StringUtils.isEmpty(keywords)){
                queryBuilder.withQuery(QueryBuilders.matchQuery("name", keywords));
//                boolQueryBuilder.must(QueryBuilders.matchQuery("name",keywords));
            }

            //根据分类查询
            if (!StringUtils.isEmpty(searchMap.get("category"))){
                boolQueryBuilder.filter(QueryBuilders.termQuery("categoryName",searchMap.get("category")));
            }

            //根据品牌查询
            if (!StringUtils.isEmpty(searchMap.get("brand"))){
                boolQueryBuilder.filter(QueryBuilders.termQuery("brandName",searchMap.get("brand")));
            }
            //根据规格查询
            //spec.手机屏幕尺寸.keyword=5寸&spec.网络.keyword=联通3G   查询条件均是以spec开头的
            for (String key : searchMap.keySet()) {
                if (key.startsWith("spec_")){
                    String value = searchMap.get(key);
                    if (!StringUtils.isEmpty(value)){
                        boolQueryBuilder.filter(QueryBuilders.termQuery("specMap." + key.substring(5) + ".keyword",searchMap.get(key)));
                    }
                }
            }

            //价格区间查询   价格 0-500元 500-1000元 ... 3000元以上
            //首先
            if (!StringUtils.isEmpty(searchMap.get("price"))){
                String str = searchMap.get("price").replace("元", "").replace("以上", "");
                String[] price = str.split("-");
                if (!StringUtils.isEmpty(price) && price.length > 0){
                    boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").gt(price[0]));
                    if (price.length >= 2 && !StringUtils.isEmpty(price[1])){
                        boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").lte(price[1]));
                    }
                }
            }
            //设置使用组合查询
            queryBuilder.withFilter(boolQueryBuilder);
        }
    }


    /**
     *  获取分组结果 根据商品的品牌分组
     * @param stringTerms
     * @return
     */
    private List<String> getStringBrandList(StringTerms stringTerms) {
        List<String> brandList = new ArrayList<>();

        for (StringTerms.Bucket bucket : stringTerms.getBuckets()) {
            String key = bucket.getKeyAsString();
            brandList.add(key);
        }

        return brandList;
    }

    /**
     *  获取分组结果 根据商品的分类分组
     * @param stringTerms
     * @return
     */
    private List<String> getStringCategoryList(StringTerms stringTerms) {
        List<String> categoryList = new ArrayList<>();

        for (StringTerms.Bucket bucket : stringTerms.getBuckets()) {
            String key = bucket.getKeyAsString();
            categoryList.add(key);
        }
        return categoryList;
    }

    /**
     *  获取分组结果 根据商品的规格分组
    * 根据商品规格进行分组    [{"电视音响效果":"小影院","电视屏幕尺寸":"20英寸","尺码":"165"},
    *                       {"电视音响效果":"立体声","电视屏幕尺寸":"20英寸","尺码":"170"},
    *                       {"电视音响效果":"立体声","电视屏幕尺寸":"60英寸","尺码":"170"}]
     *
     *
     *   转换成 ===>      [{"电视音响效果":"小影院,立体声"},"电视屏幕尺寸":"20英寸,60英寸","尺码":"165,170"]
     *      map<string,set<string>>
     *
     * @param stringTerms
     * @return
     */
    private Map getStringSpecMap(StringTerms stringTerms) {
        //选择 Set<String> 为了规格参数不重复
        Map<String, Set<String>> specMaps = new HashMap<String,Set<String>>();

        for (StringTerms.Bucket bucket : stringTerms.getBuckets()) {
            String spec = bucket.getKeyAsString();
            //获取每个商品规格
            Map<String,String> specMap = JSON.parseObject(spec,Map.class);
            //{"电视音响效果":"小影院","电视屏幕尺寸":"20英寸","尺码":"165"}
            for (Map.Entry<String, String> entry : specMap.entrySet()) {
                Set<String> set = specMaps.get(entry.getKey());
                if (set == null){
                    set = new HashSet<>();
                }
                set.add(entry.getValue());
                specMaps.put(entry.getKey(), set);
            }
        }
        return specMaps;
    }
}
