package com.changgou.goods.feign;

import com.changgou.common.pojo.Result;
import com.changgou.goods.pojo.Sku;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zhongxuwang
 * @since 1:29 下午
 */
@RequestMapping("/sku")
@FeignClient(value = "goods")
public interface SkuFeign {

    /***
     * 商品库存减少
     * @param map
     */
    @PostMapping(value = "/decr/count")
    public void decrCount(@RequestBody Map<Long, Integer> map);


    @GetMapping
    public Result<List<Sku>> findAll();

    /***
     * 根据ID查询Sku数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable Long id);
}
