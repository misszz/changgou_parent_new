package com.changgou.user.feign;

import com.changgou.common.pojo.Result;
import com.changgou.user.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhongxuwang
 * @since 2021-04-11 9:50 下午
 */
@FeignClient(name = "user")
@RequestMapping("/user")
public interface UserFeign {

    /***
     * 增加用户积分
     */
    @PostMapping("/points/add")
    public Result addPoints(@RequestParam Integer points);

    /***
     * 根据ID查询User数据
     * @param id
     * @return
     */
    @GetMapping("/load/{id}")
    public Result<User> findById(@PathVariable String id);
}
