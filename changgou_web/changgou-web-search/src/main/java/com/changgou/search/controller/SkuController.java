package com.changgou.search.controller;

import com.changgou.search.feign.SkuFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author zhongxuwang
 * @since 3:04 下午
 */
@Controller
@RequestMapping("/search")
public class SkuController {
    @Autowired
    private SkuFeign skuFeign;

    @GetMapping("/list")
    public String search(@RequestParam(required = false) Map searchMap , Model model){
        Map resultMap = skuFeign.search(searchMap);
        model.addAttribute("resultMap",resultMap);
        model.addAttribute("searchMap",searchMap);
        return "search";
    }

    @GetMapping("/item")
    public String item(){
        return "item";
    }
}
